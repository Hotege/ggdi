#if !defined(_GGDI_OBJECT_OBJECT_H_)
#define _GGDI_OBJECT_OBJECT_H_

#include <ggdi/defines.h>

namespace ggdi
{
#define class_decl(name) \
public: \
    static object::type _type_##name; \
    static const object::type* desc() { return &name::_type_##name; }

    class GGDI_API object
    {
    public:
        typedef struct _type
        {
            size_t hash_code;
            const _type* base = nullptr;
        } type;

        class_decl(object);
    public:
        void* operator new(size_t sz);
        void operator delete(void* ptr);
    protected:
        object();
        virtual ~object();
    public:
        bool is_kind_of(const type* _t) const;
        size_t get_hash_code() const;
        virtual bool is_valid() const = 0;
    };
#define class_impl(name, b) \
object::type name::_type_##name = { typeid(name).hash_code(), &b::_type_##b };
} // namespace ggdi

#endif