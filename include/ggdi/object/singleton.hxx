#if !defined(_GGDI_OBJECT_SINGLETON_HXX_)
#define _GGDI_OBJECT_SINGLETON_HXX_

#include <ggdi/object/object.h>

namespace ggdi
{
    template <typename T>
    class singleton : public object
    {
    public:
        static T& get_instance() { static T instance; return instance; }
        virtual ~singleton() {}
        singleton(const singleton&) = delete;
        singleton& operator=(const singleton&) = delete;
    protected:
        singleton() {}
    };
#define singleton_template(name) \
    private: \
        class_decl(name); \
        friend class singleton<name>; \
    public: \
        name(const name&) = delete; \
        name& operator=(const name&) = delete; \
    protected: \
        name() = default; \
    public: \
        virtual bool is_valid() const;
} // namespace ggdi

#endif