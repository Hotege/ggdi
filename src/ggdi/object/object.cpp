#include <ggdi/object/object.h>
#include <typeinfo>

namespace ggdi
{
    object::type object::_type_object = { typeid(object).hash_code(), nullptr };

    void* object::operator new(size_t sz)
    {
        return malloc((sz + 3) & ~0x3);
    }

    void object::operator delete(void* ptr)
    {
        free(ptr);
    }

    object::object()
    {
    }

    object::~object()
    {
    }

    bool object::is_kind_of(const type* _t) const
    {
        const type* t = desc();
        while (nullptr != t)
        {
            if (t == _t)
                return true;
            t = t->base;
        }
        return false;
    }

    size_t object::get_hash_code() const
    {
        return size_t(this);
    }
} // namespace ggdi
